package com.matheusmaia.ioasys.config.security;

import com.matheusmaia.ioasys.domain.Usuario;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TokenService {

    public String gerarToken(Authentication authentication) {
        Usuario logado = (Usuario) authentication.getPrincipal();
        Date now = new Date();
        return Jwts.builder().setIssuer("Api test").setSubject(logado.getId().toString())
                .setIssuedAt(now)
                .compact();
    }
}
