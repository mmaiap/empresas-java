package com.matheusmaia.ioasys.repositories;

import com.matheusmaia.ioasys.domain.Filme;
import com.matheusmaia.ioasys.domain.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmeRepository extends JpaRepository<Filme, Integer> {

}
