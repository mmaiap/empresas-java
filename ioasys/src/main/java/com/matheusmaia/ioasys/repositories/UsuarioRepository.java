package com.matheusmaia.ioasys.repositories;

import com.matheusmaia.ioasys.domain.Usuario;
import com.matheusmaia.ioasys.dto.UsuarioDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    @Query(value = "select u.id, u.nome, u.email, u.senha, u.ativo from usuario u where u.ativo = 1", nativeQuery = true)
    Page<Usuario> findByAtivo(Pageable pageable);

    @Query(value = "select u.id, u.nome, u.email, u.senha, u.ativo from usuario u where u.ativo = 1 order by u.nome", nativeQuery = true)
    List<Usuario> findByAtivoOrderByNomeAsc();

    Optional<Usuario> findByEmail(String email);
}
