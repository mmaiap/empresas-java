package com.matheusmaia.ioasys.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Data
@Entity
public class Filme implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;
    private String genero;
    private String diretor;
    private Double nota;
    private String descricao;

    @ElementCollection
    @CollectionTable(name = "votos")
    private List<Integer> votos = new ArrayList<>();

    @ElementCollection
    @CollectionTable(name = "atores")
    private List<String> atores = new ArrayList<>();

}
