package com.matheusmaia.ioasys.dto;

import com.matheusmaia.ioasys.domain.Usuario;
import lombok.Data;

import java.io.Serializable;

@Data
public class UsuarioDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String nome;
    private String email;
    private Integer ativo;

    public UsuarioDTO(Usuario obj){
        this.id = obj.getId();
        this.nome = obj.getNome();
        this.email = obj.getEmail();
        this.ativo = obj.getAtivo();
    }




}
