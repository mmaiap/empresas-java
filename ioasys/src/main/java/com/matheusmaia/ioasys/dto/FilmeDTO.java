package com.matheusmaia.ioasys.dto;

import com.matheusmaia.ioasys.domain.Filme;
import lombok.Data;

@Data
public class FilmeDTO {

    private String nome;
    private String nota;
    private String descricao;

    public FilmeDTO(Filme filme){
        this.nome = filme.getNome();
        this.nota = filme.getNota().toString();
        this.descricao = filme.getDescricao();
    }
}
