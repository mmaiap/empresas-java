package com.matheusmaia.ioasys.resources;

import com.matheusmaia.ioasys.domain.Filme;
import com.matheusmaia.ioasys.domain.Usuario;
import com.matheusmaia.ioasys.dto.FilmeDTO;
import com.matheusmaia.ioasys.dto.UsuarioDTO;
import com.matheusmaia.ioasys.services.FilmeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(value = "/filmes")
public class FilmeResource {

    @Autowired
    private FilmeService service;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> insert(@RequestBody Filme obj){
        obj = service.insert(obj);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().
                path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public ResponseEntity<Page<Filme>> findPage(@RequestParam(name="page", defaultValue = "0") Integer page,
                                                       @RequestParam(name="linesPerPage", defaultValue = "24") Integer linesPerPage,
                                                       @RequestParam(name="orderBy", defaultValue = "nome") String orderBy,
                                                       @RequestParam(name="direction", defaultValue = "ASC") String direction){
        Page<Filme> list = service.findPage(page, linesPerPage, orderBy, direction);
        return ResponseEntity.ok().body(list);

    }

    @RequestMapping(value = "/descricao/{id}", method = RequestMethod.GET)
    public ResponseEntity<FilmeDTO> descricaoDoFilme(@RequestParam Integer id){
        Filme filme = service.find(id);
        FilmeDTO filmeDTO = new FilmeDTO(filme);
        return ResponseEntity.ok(filmeDTO);
    }







}
