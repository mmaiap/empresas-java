package com.matheusmaia.ioasys.resources;

import com.matheusmaia.ioasys.domain.Usuario;
import com.matheusmaia.ioasys.dto.UsuarioDTO;
import com.matheusmaia.ioasys.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/usuarios")
public class UsuarioResource {

    @Autowired
    private UsuarioService service;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> insert(@RequestBody Usuario obj){
        obj = service.insert(obj);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().
                path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@RequestBody Usuario obj, @PathVariable Integer id){
        Usuario newObj = service.find(id);
        newObj = service.update(obj);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/excluir/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> exclusao(@PathVariable Integer id){
        Usuario obj = service.exclusao(id);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public ResponseEntity<Page<UsuarioDTO>> findPage(@PageableDefault(sort = "id", direction = Sort.Direction.ASC,
            page = 0, size = 5) Pageable paginacao){
        Page<UsuarioDTO> list = service.findPage(paginacao);
        return ResponseEntity.ok().body(list);
    }

    @RequestMapping(value = "/ativos", method = RequestMethod.GET)
    public ResponseEntity<List<UsuarioDTO>> findUsuariosOrdenados(){
        List<UsuarioDTO> list = service.findUsuarios();
        return ResponseEntity.ok().body(list);
    }





}
