package com.matheusmaia.ioasys.services;

import com.matheusmaia.ioasys.domain.Filme;
import com.matheusmaia.ioasys.repositories.FilmeRepository;
import com.matheusmaia.ioasys.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class FilmeService {

    @Autowired
    private FilmeRepository repo;

    public Filme insert(Filme obj){
        obj = repo.save(obj);
        return obj;
    }

    public Page<Filme> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
        return repo.findAll(pageRequest);
    }

    public Filme find(Integer id){
        Optional<Filme> obj = repo.findById(id);
        return obj.orElseThrow(()-> new ObjectNotFoundException("Objeto não encontrado Id: " + id));

    }




}
