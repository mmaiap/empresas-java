package com.matheusmaia.ioasys.services;

import com.matheusmaia.ioasys.domain.Usuario;
import com.matheusmaia.ioasys.dto.UsuarioDTO;
import com.matheusmaia.ioasys.repositories.UsuarioRepository;
import com.matheusmaia.ioasys.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repo;

    public Usuario find(Integer id){
        Optional<Usuario> obj = repo.findById(id);
        return obj.orElseThrow(()-> new ObjectNotFoundException("Objeto não encontrado Id: " + id));
    }

    public Usuario insert(Usuario obj){
        obj = repo.save(obj);
        return obj;
    }

    public Usuario update(Usuario obj){
        Usuario newObj = find(obj.getId());
        updateData(newObj, obj);
        return repo.save(newObj);
    }

    public Usuario exclusao(Integer id){
        Usuario newObj = find(id);
        updateStatus(newObj);
        return repo.save(newObj);
    }

    private void updateData(Usuario newObj, Usuario obj){
        newObj.setNome(obj.getNome());
        newObj.setEmail(obj.getEmail());
        newObj.setSenha(obj.getSenha());
    }

    private void updateStatus(Usuario newObj){
        newObj.setAtivo(0);
    }

    public Page<UsuarioDTO> findPage(Pageable pageable) {
        Page<Usuario> usuarios = repo.findByAtivo(pageable);
        Page<UsuarioDTO> usuariosDTO = usuarios.map(obj -> new UsuarioDTO(obj));
        return usuariosDTO;
    }

    public List<UsuarioDTO> findUsuarios(){
        List<Usuario> usuarios = repo.findByAtivoOrderByNomeAsc();
        List<UsuarioDTO> usuarioDTOS = usuarios.stream().map(obj -> new UsuarioDTO(obj)).collect(Collectors.toList());
        return usuarioDTOS;
    }

}
