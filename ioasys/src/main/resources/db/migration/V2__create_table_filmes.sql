CREATE TABLE filme(
    id bigint not null auto_increment,
    nome varchar(50) not null,
    genero varchar(50) not null,
    diretor varchar(50) not null,
    descricao varchar(100) not null,
    nota double,
    primary key(id)
)